import Grid from '@mui/material/Grid';
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Episode from './Episode';

const PersonalSection = (props) => {
    const [episodeData, setEpisodeData] = useState([]);
    const {sectionData} = props;

    const getEpisodeData = async (data) => {
        
        let episodeArr = [];
        for (let i = 0; i < data?.episode.length; i++) {
            await axios(data?.episode[i])
            .then(response => {
                episodeArr.push(response.data);
            })
        }
        setEpisodeData(episodeArr);
    };

    useEffect(() => {
        getEpisodeData(sectionData);
    }, [sectionData]);

    return (
        <div>
            {sectionData && 
            <div>
                <Grid container spacing={2} alignItems='center'>
                    <Grid item xs={3}>
                        <img
                            src={sectionData?.image}
                            loading="lazy"
                            width="100%"
                            style={{borderRadius: '50%'}}
                        />
                    </Grid>
                    <Grid item xs={9}>
                        {sectionData?.name}
                    </Grid>                
                </Grid>               
                <Grid container alignItems='center' style={{backgroundColor: 'Moccasin', padding: '10px'}}>
                    <Grid item xs={12}>
                        <h2>Personal Info</h2>
                    </Grid>
                    <Grid item xs={12}>
                        {sectionData?.status}
                    </Grid>
                    <Grid item xs={12}>
                        {sectionData?.gender}
                    </Grid>
                    <Grid item xs={12}>
                        {sectionData?.species}
                    </Grid>
                    <Grid item xs={12}>
                        {sectionData?.location?.name}
                    </Grid>
                    <Grid item xs={12}>
                        {sectionData?.origin?.name}
                    </Grid>
                    <Grid item xs={12}>
                        {sectionData?.created}
                    </Grid>
                </Grid>
                {episodeData && <Grid container alignItems='center' style={{backgroundColor: 'Moccasin', padding: '10px'}}>
                    <Grid item xs={12}>
                        <h2>Episodes</h2>
                    </Grid>
                    <Grid item xs={12}>
                        <Episode episodeData={episodeData}/>
                    </Grid>
                </Grid>}
            </div>}
        </div>
    );
}

export default PersonalSection;