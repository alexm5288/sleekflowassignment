import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';

const Episode = (props) => {
    const { episodeData } = props;

    return (
        <Table>
            <TableHead>
            <TableRow>
                <TableCell>Name</TableCell>
                <TableCell align="right">Air Date</TableCell>
                <TableCell align="right">Episode</TableCell>
                <TableCell align="right">Created Date</TableCell>
            </TableRow>
            </TableHead>
            <TableBody>
            {episodeData && episodeData.map((row) => (
                <TableRow
                key={row.id}
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                <TableCell component="th" scope="row">
                    {row.name}
                </TableCell>
                <TableCell align="right">{row.air_date}</TableCell>
                <TableCell align="right">{row.episode}</TableCell>
                <TableCell align="right">{row.created}</TableCell>
                </TableRow>
            ))}
            </TableBody>
        </Table>
    );
}

export default Episode;