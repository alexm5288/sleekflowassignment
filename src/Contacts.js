import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Button from '@mui/material/Button';
import { useNavigate } from "react-router-dom";
import PersonalSection from "./PersonalSection";

const Contacts = () => {
    const [data, setData] = useState(null);
    const [listData, setListData] = useState(null);
    const [nameVal, setNameVal] = useState(null);
    const [statusVal, setStatusVal] = useState(null);
    const [genderVal, setGenderVal] = useState(null);
    const [sectionData, setSectionData] = useState(null);
    const status = ['Alive', 'Dead', 'unknown'];
    const gender = ['Female', 'Male', 'genderless', 'unknown'];
    let navigate = useNavigate();

    const filterByName = (array) => {
        return array.filter(item => item.name.toUpperCase().includes(nameVal.toUpperCase()));
    };
    
    const filterByStatus = (array) => {
        return array.filter(item => item.status === statusVal);
    };
    
    const filterByGender = (array) => {
        return array.filter(item => item.gender === genderVal);
    };

    const clearFilter = () => {
        setNameVal('');
        setStatusVal(null);
        setGenderVal(null);
    };

    const clickContactCard = (id) => {
        setSectionData(data.find(item => item.id === id));
        navigate(`/contact/${id}`);
    };

    useEffect(() => {
        axios('https://rickandmortyapi.com/api/character')
        .then(response => {
            setData(response.data.results);
            setListData(response.data.results);
        })
    }, []);

    
    useEffect(() => {
        let result = data;
        if (nameVal)
            result = filterByName(result);
        if (statusVal)            
            result = filterByStatus(result);
        if (genderVal) 
            result = filterByGender(result);
        setListData(result);
    }, [nameVal, statusVal, genderVal]);

    return (
        <div className="contacts">
            <div style={{minWidth: '300px', backgroundColor: 'Cornsilk', padding: '10px'}}>
                <h2>Contact</h2>
                <Grid container spacing={2} alignItems='center'>
                    <Grid item xs={12}>
                        <TextField label="Search characters" variant="standard" value={nameVal} onChange={e => setNameVal(e.target.value)}/>
                    </Grid>
                    <Grid item xs={4}>
                        <FormControl fullWidth>
                            <InputLabel>Status</InputLabel>
                            <Select
                            value={statusVal}
                            label="Status"
                            onChange={e => setStatusVal(e.target.value)}
                            >
                                {status && status.map((item) => (
                                    <MenuItem value={item}>{item}</MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xs={4}>
                        <FormControl fullWidth>
                            <InputLabel>Gender</InputLabel>
                            <Select
                                value={genderVal}
                                label="Gender"
                                onChange={e => setGenderVal(e.target.value)}
                            >
                                {gender && gender.map((item) => (
                                    <MenuItem value={item}>{item}</MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xs={4}>
                        <Button onClick={clearFilter}>Clear filter</Button>
                    </Grid>
                </Grid>
                <Grid container style={{ marginTop: '15px'}} direction="column">
                {listData && listData.map((character) => (
                    <div onClick={() => clickContactCard(character.id)} className='contactCard'>
                        <Grid container spacing={2} alignItems="center">
                            <Grid item xs={3}>
                                <img
                                    src={character.image}
                                    loading="lazy"
                                    width="100%"
                                    style={{borderRadius: '50%'}}
                                />
                            </Grid>
                            <Grid item xs={9}>
                                {character.name}
                            </Grid>
                        </Grid>
                    </div>

                ))}
                </Grid>
            </div>
            <div style={{width: 'calc(100% - 300px)', minWidth: '600px', backgroundColor: 'BurlyWood', padding: '10px' }}>
                <PersonalSection sectionData={sectionData} />
            </div>
        </div>
    );
}

export default Contacts;