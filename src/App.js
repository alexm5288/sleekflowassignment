// import './App.css';
import Navbar from './Navbar';
import { Routes, Route } from "react-router-dom";
import Contacts from './Contacts';
import PersonalSection from './PersonalSection';

function App() {
  return (
    <div className="App">
      <Navbar/>
      <div className="content">
        <Routes>
          <Route path="/" element={<Contacts />} />
          <Route path="/contact" element={<Contacts />}>
          <Route
            path=":id"
            element={<PersonalSection />}
          />
          </Route>
        </Routes>
      </div>
    </div>
  );
}

export default App;
