import { Link } from "react-router-dom";

const Navbar = () => {
    return ( 
        <nav className="navbar">
            <div className="links">
                <h4>Rick and Morty</h4>
                <Link to="/contact">Contacts</Link>
            </div>
        </nav>
     );
}
 
export default Navbar;